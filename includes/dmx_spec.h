

/**
 * @{
 * @file dmx_spec.h
 * @brief Constants from the DMX (E1.11) specification.
 */

#ifndef FIRMWARE_SRC_DMX_SPEC_H_
#define FIRMWARE_SRC_DMX_SPEC_H_

/**
 * @brief The maximum size of a DMX frame, excluding the start code.
 */
#define DMX_FRAME_SIZE 512u

/**
 * @brief The Null Start Code (NSC).
 */
#define NULL_START_CODE 0x00u

/**
 * @brief The Baud rate for DMX / RDM.
 */
#define DMX_BAUD 250000u  // 250kHz

#endif  // FIRMWARE_SRC_DMX_SPEC_H_

/**
 * @}
 */
