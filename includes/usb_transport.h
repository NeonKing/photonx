
#ifndef FIRMWARE_SRC_USB_TRANSPORT_H_
#define FIRMWARE_SRC_USB_TRANSPORT_H_

#include <stdbool.h>
#include <stdint.h>

#include "constants.h"
#include "transport.h"
#include "system_definitions.h"
#include "usb/usb_device.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Initialize the USB Transport.
 * @param rx_cb The function to call when data is received from the host. This
 *   can be overridden, see below.
 *
 * If PIPELINE_TRANSPORT_RX is defined in app_pipeline.h, the macro
 * will override the rx_cb argument.
 */
void USBTransport_Initialize(TransportRxFunction rx_cb);

/**
 * @brief Perform the periodic USB layer tasks.
 *
 * This must be called within the main event loop.
 */
void USBTransport_Tasks();

/**
 * @brief Send a response to the Host.
 * @param token The frame token, this should match the request.
 * @param command The command class of the response.
 * @param rc The return code of the response.
 * @param data The iovecs with the payload data.
 * @param iov_count The number of IOVecs.
 * @returns true if the message was queued for sending. False if the device was
 * not yet configured, or is there was already a message queued.
 *
 * Only one message can be sent at a time. Until the send completes, any
 * further messages will be dropped.
 */
bool USBTransport_SendResponse(uint8_t token, Command command, uint8_t rc,
                               const IOVec* data, unsigned int iov_count);

/**
 * @brief Check if there is a write in progress
 */
bool USBTransport_WritePending();

/**
 * @brief Return the USB Device handle.
 * @returns The device handle or USB_DEVICE_HANDLE_INVALID.
 */
USB_DEVICE_HANDLE USBTransport_GetHandle();

/**
 * @brief Check if the USB driver is configured.
 * @returns true if the device if configured, false otherwise.
 */
bool USBTransport_IsConfigured();

/**
 * @brief Perform a soft reset. This aborts any outbound (write) transfers
 */
void USBTransport_SoftReset();

#ifdef __cplusplus
}
#endif

#endif  // FIRMWARE_SRC_USB_TRANSPORT_H_

/**
 * @}
 */
