

#ifndef COMMON_USB_PROPERTIES_H_
#define COMMON_USB_PROPERTIES_H_

/**
 * @file usb_properties.h
 * @brief USB Device specific constants.
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief The USB Vendor ID.
 *
 * See http://pid.codes/1209/ACEE/
 */
enum { USB_DEVICE_VENDOR_ID = 0x1209 };

/**
 * @brief The Ja Rule USB Product ID.
 *
 * See http://pid.codes/1209/ACED/
 */
enum { USB_DEVICE_MAIN_PRODUCT_ID = 0xaced };

/**
 * @brief The Ja Rule Bootloader USB Product ID.
 *
 * See http://pid.codes/1209/ACEE/
 */
enum { USB_DEVICE_BOOTLOADER_PRODUCT_ID = 0xacee };

#ifdef __cplusplus
}
#endif

#endif  // COMMON_USB_PROPERTIES_H_
