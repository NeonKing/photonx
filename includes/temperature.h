
/**
 * @defgroup temperature Temperature Sensor
 * @brief Reads the temperature sensor(s).
 *
 * @addtogroup temperature
 * @{
 * @file temperature.h
 * @brief Reads the temperature sensor(s).
 */

#ifndef FIRMWARE_SRC_TEMPERATURE_H_
#define FIRMWARE_SRC_TEMPERATURE_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief The different types of temperature sensors.
 */
typedef enum {
  TEMPERATURE_BOARD_TEMP  //!< The PCB board temp.
} TemperatureSensor;

/**
 * @brief Initialize the temperature module.
 */
void Temperature_Init();

/**
 * @brief Get the last known value for a sensor.
 * @param sensor The sensor to get the value of
 * @returns The last known value of the sensor, in 10ths of a degree.
 *
 * If the value is known this will return 0.
 */
uint16_t Temperature_GetValue(TemperatureSensor sensor);

/**
 * @brief Perform the periodic tasks.
 */
void Temperature_Tasks();

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif  // FIRMWARE_SRC_TEMPERATURE_H_
