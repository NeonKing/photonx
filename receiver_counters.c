
#include "receiver_counters.h"

static const uint16_t UNINITIALIZED_COUNTER = 0xffffu;
static const uint8_t UNINITIALIZED_CHECKSUM = 0xffu;

/*
 * @brief The counters.
 */
ReceiverCounters g_responder_counters;

// Public Functions
// ----------------------------------------------------------------------------
void ReceiverCounters_ResetCounters() {
  ReceiverCounters_ResetCommsStatusCounters();

  g_responder_counters.dmx_frames = 0u;
  g_responder_counters.asc_frames = 0u;
  g_responder_counters.rdm_frames = 0u;
  g_responder_counters.rdm_sub_start_code_invalid = 0u;
  g_responder_counters.rdm_msg_len_invalid = 0u;
  g_responder_counters.rdm_param_data_len_invalid = 0u;
  // The initial values are from E1.37-5 (draft).
  g_responder_counters.dmx_last_checksum = UNINITIALIZED_CHECKSUM;
  g_responder_counters.dmx_last_slot_count = UNINITIALIZED_COUNTER;
  g_responder_counters.dmx_min_slot_count = UNINITIALIZED_COUNTER;
  g_responder_counters.dmx_max_slot_count = UNINITIALIZED_COUNTER;
}

void ReceiverCounters_ResetCommsStatusCounters() {
  g_responder_counters.rdm_short_frame = 0u;
  g_responder_counters.rdm_length_mismatch = 0u;
  g_responder_counters.rdm_checksum_invalid = 0u;
}
