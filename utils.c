

#include "utils.h"

#include <stdlib.h>

bool StringToUInt16(const char *input, uint16_t *output) {
  long i = strtol(input, NULL, 0);  // NOLINT(runtime/int)
  if (i < 0 || i > UINT16_MAX) {
    return false;
  }
  *output = (uint16_t) i;
  return true;
}

bool StringToUInt32(const char *input, uint32_t *output) {
  long i = strtol(input, NULL, 0);  // NOLINT(runtime/int)
  if (i < 0 || i > UINT32_MAX) {
    return false;
  }
  *output = (uint32_t) i;
  return true;
}
