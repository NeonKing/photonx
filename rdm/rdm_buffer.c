
/**
 * @addtogroup rdm
 * @{
 * @file rdm_buffer.h
 * @brief The memory buffer used to construct the RDM response.
 */

#ifndef FIRMWARE_SRC_RDM_BUFFER_H_
#define FIRMWARE_SRC_RDM_BUFFER_H_

#include "rdm_buffer.h"
#include "rdm.h"

#ifdef __cplusplus
extern "C" {
#endif

static uint8_t RDM_BUFFER[RDM_MAX_FRAME_SIZE];

uint8_t *g_rdm_buffer = RDM_BUFFER;

#ifdef __cplusplus
}
#endif

/**
 * @}
 */

#endif  // FIRMWARE_SRC_RDM_BUFFER_H_
